from rest_api import views
from .views import *
from rest_framework import routers
from django.conf.urls import url, include

router = routers.DefaultRouter()
router.register(r'user_signup', views.UserSignupViewSet, basename='user_signup')
router.register(r'user_locations', views.UserLocationsViewSet, basename='user_locations')
router.register(r'all_users_list', views.AllUsersListViewSet, basename='all_users_list')
router.register(r'users_and_shops', views.UsersListViewSet, basename='users_and_shops')
router.register(r'radar', views.RadarListViewSet, basename='radar')
router.register(r'update_user_health_status', views.UserHealthStatusViewSet, basename='user_health_status')
router.register(r'statistics', views.StatisticsViewSet, basename='statistics')
router.register(r'ministry_details_page', views.MinistryDetailsViewSet, basename='ministry_details_page')
router.register(r'ministry_profile_update', views.MinistryProfileUpdateViewSet, basename='ministry_profile_update')
router.register(r'update_user_profile', views.UserProfileUpdateViewSet, basename='update_user_profile')
router.register(r'post', views.PostViewSet, basename='post')
router.register(r'comment_on_post', views.CommentOnPostViewSet, basename='comment_on_post')
router.register(r'like', views.LikeViewSet, basename='like')
router.register(r'edit_comment', views.EditCommentViewSet, basename='edit_comment')
router.register(r'get_user_details', views.ViewByUserViewSet, basename='get_user_details')
router.register(r'shop_list', views.ShopListViewSet, basename='shop_list')
router.register(r'app_links', views.AppLinksViewSet, basename='app_links')

router.register(r'approve_health_status', views.ApproveHealthStatusViewSet, basename='approve_health_status')
router.register(r'connected_people', views.ConnectedPeopleViewSet, basename='connected_people')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^login', views.LoginView.as_view(), name="login"),
    url(r'^change_password/', views.ChangePassword.as_view(), name='change_password'),
]

