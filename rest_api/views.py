from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance
from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from phonenumbers import PhoneNumber
from rest_framework.response import Response
from rest_framework import viewsets, status, permissions
from django.conf import settings
from rest_framework.generics import CreateAPIView
from rest_framework.throttling import AnonRateThrottle
from django.contrib.auth import authenticate, login
from accounts.models import PhoneToken, User, UserRole, POST, Comments, Likes, ViewBy
from rest_framework.authtoken.models import Token
from accounts.serializers import PhoneTokenCreateSerializer, PhoneTokenValidateSerializer, UserSerializer, \
    MinistrySerializer, PostSerializer, ShopSerializer, CommentSerializer, AppLinksSerializer
from rest_framework.views import APIView
from covid19.backends import AuthBackend
from accounts.models import UserLocation
from rest_api.serializers import *
from django.db.models import Q
from django.db.models import F
import datetime, calendar
from push_notifications.models import APNSDevice, GCMDevice
from rest_framework.generics import GenericAPIView
# from datetime import datetime
from math import radians, cos, sin, sqrt, atan2


# Create your views here.


class GenerateOTP(CreateAPIView, ):

    throttle_classes = (AnonRateThrottle, )
    queryset = PhoneToken.objects.all()
    serializer_class = PhoneTokenCreateSerializer

    def create(self, request, *args, **kwargs):
        ser = self.serializer_class(
            data=request.data,
            context={'request': request}
        )
        if ser.is_valid():
            token = PhoneToken.create_otp_for_number(
                request.data.get('phone_number')
            )
            if token:
                phone_token = self.serializer_class(
                    token, context={'request': request, 'phone_number': request.data.get('phone_number')}
                )
                return Response(phone_token.data, status=status.HTTP_200_OK)
            return Response({
                'detail': "you can not have more than {n} attempts per day, please try again tomorrow".format(
                    n=getattr(settings, 'PHONE_LOGIN_ATTEMPTS', 500))}, status=status.HTTP_400_BAD_REQUEST)
        elif 'phone_number' in ser.errors:
            if 'This field may not be blank' in ser.errors['phone_number']:
                return Response(
                    {'detail': 'Phone number required'}, status=status.HTTP_400_BAD_REQUEST)
            return Response(
                {'detail': ser.errors['phone_number'][0]}, status=status.HTTP_400_BAD_REQUEST)
        return Response(
            {'detail': ser.errors}, status=status.HTTP_400_BAD_REQUEST)


class UserSignupViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def create(self, request, *args, **kwargs):
        try:
            if not User.objects.filter(phone_number=request.data.get('phone_number')).exists():
                if request.data.get('role') == 1:
                    role = UserRole.objects.get(name='User')
                    user = User.objects.create(phone_number=request.data.get('phone_number'),
                                               first_name=request.data.get('name'),
                                               email=request.data.get('email'),
                                               role_id=role.id,
                                               device_type=request.data.get('device_type'),
                                               id_number=request.data.get('id_number'),
                                               device_token=request.data.get('device_token'))
                    user.set_password(request.data.get('password'))
                    user.save()
                    Token.objects.create(user=user)
                    device_type = request.data.get('device_type')
                    device_token = request.data.get('device_token')
                    if device_type == 'Android':
                        gcm_device, created = GCMDevice.objects.get_or_create(registration_id=device_token)
                        gcm_device.user = user
                        gcm_device.cloud_message_type = 'FCM'
                        gcm_device.save()

                    if device_type == 'iOS':
                        apns_device, created = APNSDevice.objects.get_or_create(registration_id=device_token)
                        apns_device.user = user
                        apns_device.save()
                    return Response(UserSerializer(user, many=False, context={'request': request}).data,
                                    status=status.HTTP_200_OK)
                elif 'role' in request.data and int(request.data.get('role')) == 2:
                    role = UserRole.objects.get(name='Ministry')
                    user = User.objects.create(phone_number=request.data.get('phone_number'),
                                               first_name=request.data.get('name'),
                                               ministry_oof_health=request.data.get('ministry_of_health'),
                                               city_name=request.data.get('city_name'),
                                               role_id=role.id,
                                               device_type=request.data.get('device_type'),
                                               device_token=request.data.get('device_token')
                                               )
                    user.set_password(request.data.get('password'))
                    user.save()
                    Token.objects.create(user=user)
                    device_type = request.data.get('device_type')
                    device_token = request.data.get('device_token')
                    if device_type == 'Android':
                        gcm_device, created = GCMDevice.objects.get_or_create(registration_id=device_token)
                        gcm_device.user = user
                        gcm_device.cloud_message_type = 'FCM'
                        gcm_device.save()

                    if device_type == 'iOS':
                        apns_device, created = APNSDevice.objects.get_or_create(registration_id=device_token)
                        apns_device.user = user
                        apns_device.save()
                    return Response(MinistrySerializer(user, many=False, context={'request': request}).data,
                                    status=status.HTTP_200_OK)
                else:
                    return Response({'detail': 'Please provide role.'}, status=status.HTTP_400_BAD_REQUEST)
            else:
                user = User.objects.get(phone_number=request.data.get('phone_number'))
                if 'role' in request.data and request.data.get('role') and int(request.data.get('role')) == 1:
                    if not user.role.name == 'Ministry':
                        return Response({'detail': "You've already registered this number with profile"},
                                        status=status.HTTP_400_BAD_REQUEST)
                if 'role' in request.data and request.data.get('role') and int(request.data.get('role')) == 2:
                    if not user.role.name == 'User':
                        return Response({'detail': "You've already registered this number with ministry profile"},
                                        status=status.HTTP_400_BAD_REQUEST)

                device_type = request.data.get('device_type')
                device_token = request.data.get('device_token')
                user.device_token = request.data.get('device_token')
                user.device_type = request.data.get('device_type')

                if device_type == 'Android':
                    gcm_device, created = GCMDevice.objects.get_or_create(registration_id=device_token)
                    gcm_device.user = user
                    gcm_device.cloud_message_type = 'FCM'
                    gcm_device.save()

                if device_type == 'iOS':
                    apns_device, created = APNSDevice.objects.get_or_create(registration_id=device_token)
                    apns_device.user = user
                    apns_device.save()
                user.save()
                if user.role and user.role.name == "User":
                    user_details = UserSerializer(user, many=False, context={'request': request})
                else:
                    user_details = MinistrySerializer(user, many=False, context={'request': request})
                return Response(user_details.data, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'detail': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)


class UserLocationsViewSet(viewsets.ModelViewSet):
    serializer_class = UserLocationSerializer
    queryset = UserLocation.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        data = request.data
        for location in data.get('locations'):
            user = request.user
            my_long_lat = str(location.get('latitude')) + " " + str(location.get('longitude'))
            my_location = str('POINT(' + my_long_lat + ')')
            now_date = datetime.datetime.today()
            location_obj = UserLocation.objects.create(latitude=location.get('latitude'),
                                                       longitude=location.get('longitude'),
                                                       location=str('POINT(' + my_long_lat + ')'),
                                                       health_status=user.health_status)
            request.user.locations.add(location_obj)
            user.latitude = location.get('latitude')
            user.longitude = location.get('longitude')
            if (request.user.current_status == "Injured" and request.user.ministry_approval) or (request.user.current_status == "Mixture" and request.user.ministry_approval):
                near_users = User.objects.filter(locations__location__distance_lt=(my_location, Distance(m=3))).exclude(health_status="Injured").exclude(health_status="Mixture").exclude(id=request.user.id).distinct()
                near_users.update(health_status="Mixture", ministry_approval=False)
                notification_text = "Your health status is Mixture."
                android_devices = GCMDevice.objects.filter(user_id__in=near_users)
                android_devices.send_message(notification_text)
                ios_devices = APNSDevice.objects.filter(user_id__in=near_users)
                ios_devices.send_message(notification_text)
            else:
                near_users = User.objects.filter(locations__location__distance_lt=(my_location, Distance(m=3)),
                                                 current_status="Injured", ministry_approval=True) or User.objects.filter(locations__location__distance_lt=(my_location, Distance(m=3)),
                                                 current_status="Mixture", ministry_approval=True)
                if near_users:
                    if request.user.current_status != "Injured" or request.user.current_status != "Mixture":
                        request.user.health_status = "Mixture"
                        request.user.ministry_approval = False
                        request.user.save()
                        notification_text = "Your health status is Mixture."
                        if request.user.device_type == "Android":
                            android_devices = GCMDevice.objects.filter(user_id=request.user.id)
                            android_devices.send_message(notification_text)

                        if request.user.device_type == "iOS":
                            ios_devices = APNSDevice.objects.filter(user_id=request.user.id)
                            ios_devices.send_message(notification_text)
            user.save()
        return Response({'detail': 'Location saved'}, status=status.HTTP_200_OK)

    def list(self, request, *args, **kwargs):
        user_id = self.request.query_params.get('user_id')
        if user_id:
            user = User.objects.get(id=user_id)
        else:
            user = request.user
        context = {
            'locations': UserLocationSerializer(user.locations, many=True, context={'request': request}).data
        }
        return Response(context, status=status.HTTP_200_OK)


class AllUsersListViewSet(viewsets.ModelViewSet):
    serializer_class = AllUsersSerializer
    queryset = User.objects.all()

    def list(self, request, *args, **kwargs):
        latitude = self.request.query_params.get('latitude')
        longitude = self.request.query_params.get('longitude')
        if latitude and longitude:
            users = User.objects.filter().exclude(latitude=None).exclude(longitude=None)
            return Response(AllUsersSerializer(users, many=True, context={'request': request}).data,
                            status=status.HTTP_200_OK)
        else:
            return Response({'detail': 'Please provide location'}, status=status.HTTP_400_BAD_REQUEST)


def getDistance(lat1, lat2, lon1, lon2):
    dlon = radians(lon2) - radians(lon1)
    dlat = radians(lat2) - radians(lat1)

    R = 6373.0

    a = sin(dlat / 2) ** 2 + cos(radians(lat1)) * cos(radians(lat2)) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    return R * c


class UsersListViewSet(viewsets.ModelViewSet):
    serializer_class = AllUsersSerializer
    queryset = User.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        latitude = self.request.query_params.get('latitude')
        longitude = self.request.query_params.get('longitude')
        if latitude and longitude:
            if not latitude == "0" and not longitude == "0" and request.user.latitude and request.user.longitude:
                # users = User.objects.filter().exclude(latitude=None).exclude(longitude=None)
                # radius_km = 50.0
                # queryset = users.annotate(
                #     radius_sqr=pow(models.F('latitude') -
                #                    request.user.latitude, 2) + pow(models.F('longitude') - request.user.longitude,
                #                                                    2)
                # ).filter(
                #     radius_sqr__lte=pow(radius_km / 9, 2)
                # )
                # shops = Shop.objects.all()
                ids = []
                point = Point(float(latitude), float(longitude))
                near_events = User.objects.filter(locations__location__distance_lt=(point, Distance(m=100))).exclude(id=request.user.id).distinct()
                # obj = User.objects.filter(id__in=ids)
                context = {
                    'users': AllUsersSerializer(near_events, many=True, context={'request': request}).data,
                    # 'shops': ShopSerializer(shops, many=True, context={'request': request}).data
                }
            else:
                context = {
                    'users': ''
                }
            return Response(context, status=status.HTTP_200_OK)
        else:
            return Response({'detail': 'Please provide location'}, status=status.HTTP_400_BAD_REQUEST)


class RadarListViewSet(viewsets.ModelViewSet):
    serializer_class = AllUsersSerializer
    queryset = User.objects.all()

    def list(self, request, *args, **kwargs):
        latitude = self.request.query_params.get('latitude')
        longitude = self.request.query_params.get('longitude')
        health_status = self.request.query_params.get('health_status')
        if latitude and longitude:
            if health_status:
                users = User.objects.filter(health_status=health_status).exclude(latitude=None).exclude(longitude=None)
                radius_km = 0.1
                queryset = users.annotate(
                    radius_sqr=pow(models.F('latitude') -
                                   request.user.latitude, 2) + pow(models.F('longitude') - request.user.longitude,
                                                                   2)
                ).filter(
                    radius_sqr__lte=pow(radius_km / 9, 2)
                )
                return Response(AllUsersSerializer(queryset, many=True, context={'request': request}).data,
                                status=status.HTTP_200_OK)
            else:
                return Response({'detail': 'Please provide status'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'detail': 'Please provide location'}, status=status.HTTP_400_BAD_REQUEST)


class LoginView(APIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    auth_obj = AuthBackend()

    def post(self, request):
        try:
            queryset = User.objects.filter(phone_number=request.data.get('phone_number'))
            if queryset:
                queryset = self.auth_obj.authenticate(username=request.data.get('phone_number'),
                                                      password=request.data.get('password'))
                if queryset:
                    login(request, queryset, backend='covid19.backend.AuthBackend')
                    device_type = request.data.get('device_type')
                    device_token = request.data.get('device_token')
                    queryset.device_type = device_type
                    queryset.device_token = device_token
                    queryset.save()
                    if device_type == 'Android':
                        gcm_device, created = GCMDevice.objects.get_or_create(registration_id=device_token)
                        gcm_device.user = queryset
                        gcm_device.cloud_message_type = 'FCM'
                        gcm_device.save()

                    if device_type == 'iOS':
                        apns_device, created = APNSDevice.objects.get_or_create(registration_id=device_token)
                        apns_device.user = queryset
                        apns_device.save()
                    if queryset.role and queryset.role.name == 'User':
                        return Response(UserSerializer(queryset, many=False, context={"request": request}).data,
                                        status=status.HTTP_200_OK)
                    elif queryset.role and queryset.role.name == 'Ministry':
                        return Response(MinistrySerializer(queryset, many=False, context={"request": request}).data,
                                        status=status.HTTP_200_OK)
                    else:
                        return Response(UserSerializer(queryset, many=False, context={"request": request}).data,
                                        status=status.HTTP_200_OK)
                else:
                    return Response({'error': 'Incorrect Password'}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            if 'is not a valid phone number' in e.args[0]:
                return Response({'error': 'Invalid phone number'}, status=status.HTTP_400_BAD_REQUEST)
        return Response({'error': 'Phone number not register'}, status=status.HTTP_400_BAD_REQUEST)


class UserHealthStatusViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        pk = request.data.get("user_id")
        if User.objects.filter(id=pk).exists():
            user = User.objects.get(id=pk)
            user.ministry_approval = False
            if request.data.get("health_status"):
                user.health_status = request.data.get("health_status")
            if request.data.get("additional_email"):
                user.additional_email = request.data.get("additional_email")
            if request.data.get("additional_number"):
                user.additional_number = request.data.get("additional_number")
            user.save()
            notification_text = user.first_name + " has updated their health status to " \
                                                  "" + request.data.get("health_status") + ".\nPlease review!"

            # ministries = list(User.objects.filter(role__name="Ministry").values_list('id', flat=True))
            # if user.device_type == "Android":
                # android_devices = GCMDevice.objects.filter(user_id__in=ministries)
                # android_devices.send_message(notification_text)

            # if user.device_type == 'iOS':
                # ios_devices = APNSDevice.objects.filter(user_id__in=ministries)
                # ios_devices.send_message(notification_text)

            return Response(UserSerializer(user, many=False, context={"request": request}).data,
                            status=status.HTTP_200_OK)
        else:
            return Response({'detail': 'No record found'}, status=status.HTTP_400_BAD_REQUEST)


class StatisticsViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        data = self.request.query_params
        health_condition = data.get('health_condition')
        filter_type = data.get('filter_type', 'Day')
        see_all = data.get('see_all')
        if health_condition:
            country_code = "+" + str(request.user.phone_number.country_code) if request.user.phone_number else "+966"
            users = User.objects.filter(health_status=health_condition, phone_number__startswith=country_code).exclude(role__name="Ministry").exclude(is_superuser=True).exclude(id=request.user.id).order_by('-updated_at')
            if not see_all == 'True':
                users = users
                dates = []
                if filter_type == "Day":
                    today = datetime.datetime.today()
                    first_day = datetime.datetime.today().replace(day=1)
                    num_days = calendar.monthrange(today.year, today.month)[1]
                    # diff = today - first_day
                    # date_array = (datetime.datetime.today() - datetime.timedelta(days=x) for x in range(0, diff.days+2))
                    date_array = [datetime.date(today.year, today.month, day) for day in range(1, num_days+1)]
                    for obj in date_array:
                        print(obj)
                        json_data = {}
                        if health_condition == "Healthy":
                            query = Q(is_healthy=True, healthy_at=obj)
                        elif health_condition == "Mixture":
                            query = Q(is_mixture=True, mixture_at=obj)
                        elif health_condition == "Suspicious Symptoms":
                            query = Q(is_suspicious=True, suspicious_at=obj)
                        elif health_condition == "Injured":
                            query = Q(is_injured=True, injured_at=obj)
                        else:
                            query = Q(is_recovered=True, recovered_at=obj)
                        json_data['date'] = obj
                        json_data['count'] = User.objects.filter(query, role__name="User", phone_number__startswith=country_code).exclude(role__name="Ministry").exclude(is_superuser=True).exclude(id=request.user.id).count()
                        dates.append(json_data)
                context = {
                    'filter_type': filter_type,
                    'health_condition': health_condition,
                    'dates': dates,
                    'users': UserSerializer(users, many=True, context={"request": request}).data,
                }
                return Response(context, status=status.HTTP_200_OK)

            else:
                if health_condition == "Healthy":
                    users = users.exclude(is_healthy=True)
                elif health_condition == "Mixture":
                    users = users.exclude(is_mixture=True)
                elif health_condition == "Suspicious Symptoms":
                    users = users.exclude(is_suspicious=True)
                elif health_condition == "Injured":
                    users = users.exclude(is_injured=True)
                else:
                    users = users.exclude(is_recovered=True)
                return Response(UserSerializer(users, many=True, context={"request": request}).data,
                                status=status.HTTP_200_OK)
        return Response({'error': 'Please provide health condition'}, status=status.HTTP_400_BAD_REQUEST)


class MinistryDetailsViewSet(viewsets.ModelViewSet):
    serializer_class = MinistrySerializer
    queryset = User.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        ministry = User.objects.get(id=request.user.id)
        if ministry:
            country_code = "+" + str(request.user.phone_number.country_code) if request.user.phone_number else "+966"
            healthy_count = User.objects.filter(health_status="Healthy", phone_number__startswith=country_code).exclude(role__name="Ministry").exclude(is_superuser=True).exclude(id=request.user.id).count()
            recovered_count = User.objects.filter(health_status="Recovered", phone_number__startswith=country_code).exclude(role__name="Ministry").exclude(is_superuser=True).exclude(id=request.user.id).count()
            symptoms_count = User.objects.filter(health_status="Suspicious Symptoms", phone_number__startswith=country_code).exclude(role__name="Ministry").exclude(is_superuser=True).exclude(id=request.user.id).count()
            mixture_count = User.objects.filter(health_status="Mixture", phone_number__startswith=country_code).exclude(role__name="Ministry").exclude(is_superuser=True).exclude(id=request.user.id).count()
            injured_count = User.objects.filter(health_status="Injured", phone_number__startswith=country_code).exclude(role__name="Ministry").exclude(is_superuser=True).exclude(id=request.user.id).count()
            context = {
                "ministry": MinistrySerializer(ministry, many=False, context={"request": request}).data,
                "healthy_count": healthy_count,
                "recovered_count": recovered_count,
                "symptoms_count": symptoms_count,
                "mixture_count": mixture_count,
                "injured_count": injured_count
            }
            return Response(context, status=status.HTTP_200_OK)
        return Response({'error': 'Incorrect Health Condition'}, status=status.HTTP_400_BAD_REQUEST)


class MinistryProfileUpdateViewSet(viewsets.ModelViewSet):
    serializer_class = MinistrySerializer
    queryset = User.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def update(self, request, pk, *args, **kwargs):
        user = User.objects.get(id=pk)
        if request.data.get("profile_pic"):
            user.profile_pic = request.data.get("profile_pic")
        if request.data.get("name"):
            user.first_name = request.data.get("name")
        if request.data.get("email"):
            user.email = request.data.get("email")
        user.save()
        return Response(MinistrySerializer(user, many=False, context={"request": request}).data,
                        status=status.HTTP_200_OK)


class UserProfileUpdateViewSet(viewsets.ModelViewSet):
    serializer_class = MinistrySerializer
    queryset = User.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def update(self, request, pk, *args, **kwargs):
        user = User.objects.get(id=pk)
        if request.data.get("profile_pic"):
            user.profile_pic = request.data.get("profile_pic")
        if request.data.get("name"):
            user.first_name = request.data.get("name")
        user.save()
        return Response(UserSerializer(user, many=False, context={"request": request}).data,
                        status=status.HTTP_200_OK)


class PostViewSet(viewsets.ModelViewSet):
    serializer_class = PostSerializer
    queryset = POST.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        obj = POST.objects.filter(posted_by=request.user).order_by("-posted_on")
        return Response(PostSerializer(obj, many=True, context={"request": request}).data,
                        status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        post = request.data.get("post")
        if post:
            POST.objects.create(posted_by=request.user, post=post)
            all_post = POST.objects.filter(posted_by=request.user)
            return Response(PostSerializer(all_post, many=True, context={"request": request}).data,
                            status=status.HTTP_200_OK)

    def update(self, request, pk, *args, **kwargs):
        try:
            obj = POST.objects.get(id=pk)
            obj.post = request.data.get("post")
            obj.save()
            all_post = POST.objects.filter(posted_by=request.user)
            return Response(PostSerializer(all_post, many=True, context={"request": request}).data,
                            status=status.HTTP_200_OK)
        except:
            return Response({'error': 'Post not exits.'}, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk, *args, **kwargs):
        try:
            POST.objects.get(id=pk).delete()
            all_post = POST.objects.filter(posted_by=request.user)
            return Response(PostSerializer(all_post, many=True, context={"request": request}).data,
                            status=status.HTTP_200_OK)
        except:
            return Response({'error': 'Post not exits.'}, status=status.HTTP_400_BAD_REQUEST)


class CommentOnPostViewSet(viewsets.ModelViewSet):
    serializer_class = CommentSerializer
    queryset = Comments.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        post_id = request.data.get("post_id")
        comment = request.data.get("comment")
        if post_id:
            obj = POST.objects.get(id=post_id)
            obj.comments.create(comment=comment, commented_by=request.user)
            obj.save()
            notification_text = request.user.first_name + " is comment on your post."
            if obj.posted_by.device_type == "Android":
                android_devices = GCMDevice.objects.filter(user_id=obj.posted_by.id)
                android_devices.send_message(notification_text)

            if obj.posted_by.device_type == "iOS":
                ios_devices = APNSDevice.objects.filter(user_id=obj.posted_by.id)
                ios_devices.send_message(notification_text)
            return Response(PostSerializer(obj, many=False, context={"request": request}).data,
                            status=status.HTTP_200_OK)
        return Response({'error': 'Incorrect Details.'}, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk, *args, **kwargs):
        if Comments.objects.filter(id=pk).exists():
            Comments.objects.filter(id=pk).delete()
            return Response({'detail': 'Comment Deleted'}, status=status.HTTP_200_OK)
        else:
            return Response({'detail': 'No record found'}, status=status.HTTP_400_BAD_REQUEST)


class LikeViewSet(viewsets.ModelViewSet):
    serializer_class = PostSerializer
    queryset = POST.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        post_id = request.data.get("post_id")
        comment_id = request.data.get("comment_id")
        if post_id:
            obj = POST.objects.get(id=post_id)
            if obj.likes.filter(liked_by=request.user).exists():
                obj.likes.filter(liked_by=request.user).delete()
                notification_text = request.user.first_name + " is dislike your comment."
                if obj.posted_by.device_type == "Android":
                    android_devices = GCMDevice.objects.filter(user_id=obj.posted_by.id)
                    android_devices.send_message(notification_text)

                if obj.posted_by.device_type == "iOS":
                    ios_devices = APNSDevice.objects.filter(user_id=obj.posted_by.id)
                    ios_devices.send_message(notification_text)
            else:
                obj.likes.create(liked_by=request.user)
                notification_text = request.user.first_name + " is like your comment."
                if obj.posted_by.device_type == "Android":
                    android_devices = GCMDevice.objects.filter(user_id=obj.posted_by.id)
                    android_devices.send_message(notification_text)

                if obj.posted_by.device_type == "iOS":
                    ios_devices = APNSDevice.objects.filter(user_id=obj.posted_by.id)
                    ios_devices.send_message(notification_text)
            obj.save()
            return Response(PostSerializer(obj, many=False, context={"request": request}).data,
                            status=status.HTTP_200_OK)
        if comment_id:
            obj_comment = Comments.objects.get(id=comment_id)
            if obj_comment.likes.filter(liked_by=request.user).exists():
                obj_comment.likes.filter(liked_by=request.user).delete()
                notification_text = request.user.first_name + " is dislike your comment."
                if obj_comment.commented_by.device_type == "Android":
                    android_devices = GCMDevice.objects.filter(user_id=obj_comment.commented_by.id)
                    android_devices.send_message(notification_text)

                if obj_comment.commented_by.device_type == "iOS":
                    ios_devices = APNSDevice.objects.filter(user_id=obj_comment.commented_by.id)
                    ios_devices.send_message(notification_text)
            else:
                obj_comment.likes.create(liked_by=request.user)
                notification_text = request.user.first_name + " is like your comment."
                if obj_comment.commented_by.device_type == "Android":
                    android_devices = GCMDevice.objects.filter(user_id=obj_comment.commented_by.id)
                    android_devices.send_message(notification_text)

                if obj_comment.commented_by.device_type == "iOS":
                    ios_devices = APNSDevice.objects.filter(user_id=obj_comment.commented_by.id)
                    ios_devices.send_message(notification_text)
            obj_comment.save()
            return Response(CommentSerializer(obj_comment, many=False, context={"request": request}).data,
                            status=status.HTTP_200_OK)
        return Response({'error': 'Incorrect Details.'}, status=status.HTTP_400_BAD_REQUEST)


class EditCommentViewSet(viewsets.ModelViewSet):
    serializer_class = PostSerializer
    queryset = POST.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def update(self, request, pk, *args, **kwargs):
        try:
            obj = Comments.objects.get(id=pk)
            obj.comment = request.data.get("comment")
            obj.save()
            return Response(CommentSerializer(obj, many=False, context={"request": request}).data,
                            status=status.HTTP_200_OK)
        except:
            return Response({'error': 'Post not exits.'}, status=status.HTTP_400_BAD_REQUEST)


class ViewByUserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        user_id = request.data.get("user_id")
        if user_id:
            user_obj = User.objects.get(id=user_id)
            post_obj = POST.objects.filter(posted_by=user_obj)
            for obj in post_obj:
                if not obj.view.filter(comment_view_by=request.user).exists():
                    obj.view.create(comment_view_by=request.user)
                obj.save()
            context = {
                "user": UserSerializer(user_obj, many=False, context={"request": request}).data,
                "post": PostSerializer(post_obj, many=True, context={"request": request}).data
            }
            return Response(context, status=status.HTTP_200_OK)
        return Response({'error': 'Incorrect Details.'}, status=status.HTTP_400_BAD_REQUEST)


class ApproveHealthStatusViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.filter(health_status="Injured", is_injured=False).order_by('-updated_at')
    permission_classes = (permissions.IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        user_id = request.data.get('user_id')
        approved = request.data.get('approved')
        health_status = request.data.get("health_status")
        if user_id:
            if User.objects.filter(id=user_id).exists():
                user = User.objects.get(id=user_id)
                user.is_healthy = False
                user.is_mixture = False
                user.is_suspicious = False
                user.is_injured = False
                user.is_recovered = False
                user.health_status = health_status
                user.current_status = health_status
                user.ministry_approval = request.data.get('approved')

                if health_status == "Healthy":
                    user.is_healthy = approved
                    user.healthy_at = datetime.datetime.today()
                    user.save()
                elif health_status == "Mixture":
                    user.is_mixture = approved
                    user.mixture_at = datetime.datetime.today()
                    user.save()
                elif health_status == "Suspicious Symptoms":
                    user.is_suspicious = approved
                    user.suspicious_at = datetime.datetime.today()
                    user.save()
                elif health_status == "Injured":
                    user.is_injured = approved
                    user.injured_at = datetime.datetime.today()
                    user.save()
                elif health_status == "Recovered":
                    user.is_recovered = approved
                    user.recovered_at = datetime.datetime.today()
                    user.save()

                if health_status == "Mixture":
                    if approved:
                        user_locations = user.locations.all().last()
                        near_users = User.objects.filter(
                            locations__location__distance_lt=(user_locations.location, Distance(m=3))).exclude(
                            current_status="Mixture").exclude(health_status="Mixture").exclude(
                            current_status="Injured").exclude(health_status="Injured").exclude(id=user_id).exclude(role__name="Ministry").exclude(is_superuser=True)
                        if near_users:
                            near_users.update(health_status="Mixture", ministry_approval=False)
                            notification_text = "Your health status is Mixture."
                            android_devices = GCMDevice.objects.filter(user_id__in=near_users)
                            android_devices.send_message(notification_text)
                            ios_devices = APNSDevice.objects.filter(user_id__in=near_users)
                            ios_devices.send_message(notification_text)

                if health_status == "Injured":
                    if approved:
                        user_locations = user.locations.all().last()
                        near_users = User.objects.filter(
                            locations__location__distance_lt=(user_locations.location, Distance(m=3))).exclude(
                            current_status="Mixture").exclude(health_status="Mixture").exclude(
                            current_status="Injured").exclude(health_status="Injured").exclude(id=user_id).exclude(role__name="Ministry").exclude(is_superuser=True)
                        if near_users:
                            near_users.update(health_status="Mixture", ministry_approval=False)
                            notification_text = "Your health status is Mixture."
                            android_devices = GCMDevice.objects.filter(user_id__in=near_users)
                            android_devices.send_message(notification_text)
                            ios_devices = APNSDevice.objects.filter(user_id__in=near_users)
                            ios_devices.send_message(notification_text)
                notification_text = "Your status has been approved"
                android_devices = GCMDevice.objects.filter(user_id=user_id)
                android_devices.send_message(notification_text)
                ios_devices = APNSDevice.objects.filter(user_id=user_id)
                ios_devices.send_message(notification_text)
                return Response({'detail': 'Status approved'}, status=status.HTTP_200_OK)
            else:
                return Response({'detail': 'No record found'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'detail': 'Please provide user id'}, status=status.HTTP_400_BAD_REQUEST)


class ConnectedPeopleViewSet(viewsets.ModelViewSet):
    serializer_class = AllUsersSerializer
    queryset = User.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        user_id = self.request.query_params.get('user_id')
        if user_id:
            user_obj = User.objects.get(id=user_id)
            now = datetime.datetime.now()
            yesterday = now - datetime.timedelta(days=1)
            user_locations = user_obj.locations.all().last()
            near_users = User.objects.filter(locations__location__distance_lt=(user_locations.location, Distance(m=3))).exclude(id=user_obj.id).exclude(role__name="Ministry").exclude(is_superuser=True).distinct()
            print(user_locations)
            # users = User.objects.filter(locations__created_at__gte=yesterday).exclude(id=user_obj.id)
            # connected_users = []
            # for user in users:
            #     locations = user.locations.filter(created_at__gte=yesterday).values_list('latitude', 'longitude')
            #     for location in locations:
            #         if location in user_locations:
            #             connected_users.append(user.id)

            return Response(AllUsersSerializer(User.objects.filter(id__in=near_users), many=True, context={'request': request}).data,
                            status=status.HTTP_200_OK)
        else:
            return Response({'detail': 'Please provide user details'}, status=status.HTTP_400_BAD_REQUEST)


class ShopListViewSet(viewsets.ModelViewSet):
    serializer_class = ShopSerializer
    queryset = Shop.objects.all()
    permission_classes = (permissions.IsAuthenticated,)


class AppLinksViewSet(viewsets.ModelViewSet):
    serializer_class = AppLinksSerializer
    queryset = AppLinks.objects.all()
    permission_classes = (permissions.IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        language = self.request.query_params.get('language')
        link = "https://covid19.moh.gov.sa/"
        apps = AppLinks.objects.all().order_by("id")
        context = {
            'apps': AppLinksSerializer(apps, many=True, context={'request': request, 'language': language}).data,
            'link': link
        }
        return Response(context, status=status.HTTP_200_OK)


class ChangePassword(GenericAPIView):
    queryset = User.objects.all()
    serializer_class = User

    def post(self, request):
        queryset = User.objects.filter(phone_number=request.data['phone_number'])
        if queryset:
            user = User.objects.get(phone_number=request.data['phone_number'])
            user.set_password(request.data['new_password'])
            user.save()
            serializer = UserSerializer(user, many=False, context={'request': request})
            data = serializer.data
            return Response(data, status=status.HTTP_200_OK)
        else:
            return Response({'status': False, 'detail': 'Invalid credentials.'}, status=status.HTTP_400_BAD_REQUEST)