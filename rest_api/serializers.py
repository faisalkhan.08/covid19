from rest_framework import serializers
from accounts.models import *


class UserLocationSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserLocation
        fields = ("latitude", "longitude", "created_at", "health_status")


class AllUsersSerializer(serializers.ModelSerializer):
    profile_pic = serializers.SerializerMethodField()
    phone_number = serializers.SerializerMethodField()
    country_code = serializers.SerializerMethodField()
    health_status = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ("id", "first_name", "latitude", "longitude", "health_status", "profile_pic", "phone_number", "country_code")

    def get_profile_pic(self, instance):
        request = self.context.get('request')
        return request.build_absolute_uri(instance.profile_pic.url) if instance.profile_pic else ""

    def get_phone_number(self, instance):
        return str(instance.phone_number.national_number) if instance.phone_number else ""

    def get_health_status(self, instance):
        return instance.health_status if instance.ministry_approval else instance.current_status

    def get_country_code(self, instance):
        return "+" + str(instance.phone_number.country_code) if instance.phone_number else ""

    def to_representation(self, instance):
        request = self.context.get('request')
        data = super().to_representation(instance)
        if not data['phone_number']:
            data['phone_number'] = None
        return data
