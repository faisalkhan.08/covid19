from django.shortcuts import render, redirect
from django.contrib import messages
from accounts.models import Shop, UserRole, User
from rest_framework.authtoken.models import Token


# Create your views here.


def login_page(request):
    return render(request, 'login.html')


def shop_list(request):
    role = UserRole.objects.get(name='Ministry')
    ministry = User.objects.filter(role_id=role.id)
    shops = Shop.objects.all()
    return render(request, 'shops_list.html', {'shops': shops, 'ministry': ministry})


def shop_details(request, shop_id):
    if request.method == "GET":
        details = Shop.objects.get(id=shop_id)
        context = {
            "shop_details": details
        }
        return render(request, "shop_details.html", context)


def create_shop_page(request):
    return render(request, "create_shop.html")


def ministry_create(request):
    if request.method == "POST":
        try:
            data = request.POST
            phone_number = data.get('country_code') + data.get('phone_number')
            role = UserRole.objects.get(name='Ministry')
            user = User.objects.create(phone_number=phone_number, first_name=data.get('name'),
                                       ministry_oof_health=data.get('ministry_of_health'),
                                       city_name=data.get('city_name'),
                                       role_id=role.id
                                       )
            user.set_password(data.get('password'))
            user.save()
            Token.objects.create(user=user)
            return redirect('admin_panel:ministry_list')
        except Exception as e:
            messages.warning(request, "Form have some error" + str(e))
            return redirect('admin_panel:ministry_list')
    return render(request, 'ministry_create.html')


def ministry_list(request):
    role = UserRole.objects.get(name='Ministry')
    ministry = User.objects.filter(role_id=role.id)
    shops = Shop.objects.all()
    return render(request, 'ministry_list.html', {'ministry': ministry, 'shops': shops})


def update_shop_page(request, shop_id):
    shops = Shop.objects.get(id=shop_id)
    return render(request, 'update_shop.html', {'shops': shops})


def create_shop(request):
    if request.method == "POST":
        try:
            obj = Shop.objects.create(shop_name=request.POST.get('shop_name'),
                                      shop_category=request.POST.get('shop_category'),
                                      health_status=request.POST.get('health_status'),
                                      shop_latitude=request.POST.get('shop_latitude'),
                                      shop_longitude=request.POST.get('shop_longitude'),
                                      address=request.POST.get('address'),
                                      phone_number=request.POST.get('phone'),
                                      country_code=request.POST.get('country_code'))
            shop_pic = request.FILES.get("shop_pic")
            obj.shop_pic = shop_pic
            obj.save()
            return redirect('/admin_panel/shop_list/')
        except Exception as e:
            messages.warning(request, "Form have some error" + str(e))
            return redirect('/admin_panel/shop_list/')


def update_shop(request, shop_id):
    if request.method == "POST":
        try:
            obj = Shop.objects.get(id=shop_id)
            obj.shop_name = request.POST.get('shop_name')
            obj.shop_category = request.POST.get('shop_category')
            obj.health_status = request.POST.get('health_status')
            obj.shop_latitude = request.POST.get('shop_latitude')
            obj.shop_longitude = request.POST.get('shop_longitude')
            obj.phone_number = request.POST.get('phone')
            obj.country_code = request.POST.get('country_code')
            shop_pic = request.FILES.get("shop_pic")
            if shop_pic:
                obj.shop_pic = shop_pic
            obj.save()
            return redirect('/admin_panel/shop_list/')
        except Exception as e:
            messages.warning(request, "Form have some error" + str(e))
            return redirect('/admin_panel/shop_list/')