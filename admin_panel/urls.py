from django.urls import path, include
from . import views
app_name = 'admin_panel'

urlpatterns = [
    path('shop_list/', views.shop_list, name='shop_list'),
    path('shop_details/<int:shop_id>/', views.shop_details, name='shop_details'),
    path('create_shop/', views.create_shop, name='create_shop'),
    path('create_shop_page/', views.create_shop_page, name='create_shop_page'),
    path('add_ministry/', views.ministry_create, name='add_ministry'),
    path('ministry_list/', views.ministry_list, name='ministry_list'),
    path('update_shop_page/<int:shop_id>/', views.update_shop_page, name='update_shop_page'),
    path('update_shop/<int:shop_id>/', views.update_shop, name='update_shop'),
]
