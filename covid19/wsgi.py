"""
WSGI config for covid19 project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os
import sys
from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'covid19.settings')
sys.path.append('/var/www/covid19/covid19')
sys.path.append('/var/www/covid19/env/lib/python3.6/site-packages')
application = get_wsgi_application()




