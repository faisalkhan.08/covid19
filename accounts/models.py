from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from phonenumber_field.modelfields import PhoneNumberField
from django.conf import settings
import datetime
import hashlib
import os
from rest_framework.authtoken.models import Token
from django.contrib.gis.db import models


class PhoneToken(models.Model):
    phone_number = PhoneNumberField(blank=True, null=True)
    otp = models.CharField(max_length=40, editable=False)
    timestamp = models.DateTimeField(auto_now_add=True, editable=False)
    attempts = models.IntegerField(default=0)
    used = models.BooleanField(default=False)

    class Meta:
        verbose_name = "OTP Token"

    def __str__(self):
        return "{} - {}".format(self.phone_number, self.otp)

    @classmethod
    def create_otp_for_number(cls, number):
        today_min = datetime.datetime.combine(datetime.date.today(), datetime.time.min)
        today_max = datetime.datetime.combine(datetime.date.today(), datetime.time.max)
        otps = cls.objects.filter(phone_number=number, timestamp__range=(today_min, today_max))
        if otps.count() <= getattr(settings, 'PHONE_LOGIN_ATTEMPTS', 10):
            otp = cls.generate_otp(length=getattr(settings, 'PHONE_LOGIN_OTP_LENGTH', 4))
            phone_token = PhoneToken(phone_number=number, otp=otp)
            phone_token.save()
            return phone_token
        else:
            return False

    @classmethod
    def generate_otp(cls, length=4):
        hash_algorithm = getattr(settings, 'PHONE_LOGIN_OTP_HASH_ALGORITHM', 'sha256')
        m = getattr(hashlib, hash_algorithm)()
        m.update(getattr(settings, 'SECRET_KEY', None).encode('utf-8'))
        m.update(os.urandom(16))
        otp = str(int(m.hexdigest(), 16))[-length:]
        return otp


class UserRole(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class UserManager(BaseUserManager):
    def create_user(self, email, phone_number, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        print(password)

        user = self.model(
            email=self.normalize_email(email),
            phone_number=phone_number,
            password=password,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, phone_number, email, password=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        print(password)
        user = self.create_user(
            email,
            phone_number=phone_number,
            password=password,
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        Token.objects.create(user=user)
        return user


class UserLocation(models.Model):
    Condition_Status = (
        ('Recovered', 'Recovered'),
        ('Healthy', 'Healthy'),
        ('Suspicious Symptoms', 'Suspicious Symptoms'),
        ('Mixture', 'Mixture'),
        ('Injured', 'Injured'),
    )
    latitude = models.CharField(max_length=255, null=True, blank=True)
    longitude = models.CharField(max_length=255, null=True, blank=True)
    location = models.PointField(null=True, default='POINT(0.0 0.0)')
    health_status = models.CharField(default="Healthy", max_length=254, null=True, blank=True, choices=Condition_Status)
    created_at = models.DateTimeField(auto_now_add=True)


class User(AbstractUser):
    Condition_Status = (
        ('Recovered', 'Recovered'),
        ('Healthy', 'Healthy'),
        ('Suspicious Symptoms', 'Suspicious Symptoms'),
        ('Mixture', 'Mixture'),
        ('Injured', 'Injured'),
    )

    objects = UserManager()

    email = models.CharField(max_length=254, null=True, blank=True)
    username = models.CharField(max_length=254, null=True, blank=True)
    first_name = models.CharField(max_length=256, blank=True, null=True)
    last_name = models.CharField(max_length=256, blank=True, null=True)
    phone_number = PhoneNumberField(blank=True, null=True, unique=True)
    profile_pic = models.FileField(upload_to='profile_pic/', null=True, blank=True)
    role = models.ForeignKey(UserRole, related_name='user_role', null=True, blank=True, on_delete=models.CASCADE)
    health_status = models.CharField(default="Healthy", max_length=254, null=True, blank=True, choices=Condition_Status)
    current_status = models.CharField(default="Healthy", max_length=254, null=True, blank=True, choices=Condition_Status)
    latitude = models.CharField(max_length=254, null=True, blank=True)
    longitude = models.CharField(max_length=254, null=True, blank=True)
    locations = models.ManyToManyField(UserLocation, null=True, blank=True)

    id_number = models.CharField(max_length=254, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # Device Details
    device_type = models.CharField(max_length=10, blank=True, null=True)
    device_token = models.CharField(max_length=500, blank=True, null=True)

    # Ministry Of Health
    ministry_oof_health = models.CharField(max_length=500, blank=True, null=True)
    city_name = models.CharField(max_length=500, blank=True, null=True)
    state_name = models.CharField(max_length=500, blank=True, null=True)

    additional_number = models.CharField(max_length=50, blank=True, null=True)
    additional_email = models.CharField(max_length=50, blank=True, null=True)

    ministry_approval = models.BooleanField(default=False)

    is_healthy = models.BooleanField(default=False)
    healthy_at = models.DateField(null=True, blank=True, default=datetime.datetime.now())

    is_mixture = models.BooleanField(default=False)
    mixture_at = models.DateField(null=True, blank=True, default=datetime.datetime.now())

    is_injured = models.BooleanField(default=False)
    injured_at = models.DateField(null=True, blank=True, default=datetime.datetime.now())

    is_suspicious = models.BooleanField(default=False)
    suspicious_at = models.DateField(null=True, blank=True, default=datetime.datetime.now())

    is_recovered = models.BooleanField(default=False)
    recovered_at = models.DateField(null=True, blank=True, default=datetime.datetime.now())

    USERNAME_FIELD = 'phone_number'
    # REQUIRED_FIELDS = ['username', 'email']

    def __str__(self):
        return str(self.phone_number) if self.phone_number else "NA"


class Likes(models.Model):
    liked_by = models.ForeignKey(User, related_name='post_liked_by', blank=True, null=True, on_delete=models.CASCADE)
    liked_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-liked_on']


class ViewBy(models.Model):
    count = models.CharField(max_length=250, blank=True, null=True)
    comment_view_by = models.ForeignKey(User, related_name='comment_view_by', blank=True, null=True,
                             on_delete=models.CASCADE)


class Comments(models.Model):
    comment = models.TextField(blank=True, null=True)
    likes = models.ManyToManyField(Likes, related_name='comment_likes', blank=True, null=True)
    commented_by = models.ForeignKey(User, related_name='post_commented_by', blank=True, null=True,
                                     on_delete=models.CASCADE)
    commented_on = models.DateTimeField(auto_now_add=True)
    view_by = models.ManyToManyField(ViewBy, related_name='comment_view', blank=True, null=True)

    class Meta:
        ordering = ['-commented_on']


class POST(models.Model):

    post = models.TextField(blank=True, null=True)
    likes = models.ManyToManyField(Likes, related_name='post_likes', blank=True, null=True)
    comments = models.ManyToManyField(Comments, related_name='post_comments', blank=True, null=True)
    posted_by = models.ForeignKey(User, related_name='post_owner', blank=True, null=True, on_delete=models.CASCADE)
    posted_on = models.DateTimeField(auto_now_add=True)
    view = models.ManyToManyField(ViewBy, related_name='view', blank=True, null=True)

    class Meta:
        ordering = ['-posted_on']


class Shop(models.Model):
    Condition_Status = (
        ('Recovered', 'Recovered'),
        ('Healthy', 'Healthy'),
        ('Suspicious Symptoms', 'Suspicious Symptoms'),
        ('Mixture', 'Mixture'),
        ('Injured', 'Injured'),
    )

    shop_name = models.CharField(max_length=256, blank=True, null=True)
    shop_category = models.CharField(max_length=256, blank=True, null=True)
    shop_latitude = models.CharField(max_length=254, null=True, blank=True)
    shop_longitude = models.CharField(max_length=254, null=True, blank=True)
    shop_pic = models.FileField(upload_to='shop_pic/', null=True, blank=True)
    health_status = models.CharField(max_length=254, null=True, blank=True, choices=Condition_Status)
    address = models.CharField(max_length=256, blank=True, null=True)
    phone_number = models.CharField(max_length=256, blank=True, null=True)
    country_code = models.CharField(max_length=256, blank=True, null=True)


class AppLinks(models.Model):
    android_link = models.CharField(max_length=254, null=True, blank=True)
    ios_link = models.CharField(max_length=254, null=True, blank=True)
    color = models.CharField(max_length=254, null=True, blank=True)
    name_english = models.CharField(max_length=254, null=True, blank=True)
    name_arabic = models.CharField(max_length=254, null=True, blank=True)
    description_english = models.TextField(null=True, blank=True)
    description_arabic = models.TextField(null=True, blank=True)
    image = models.FileField(upload_to='app_pics/', null=True, blank=True)
