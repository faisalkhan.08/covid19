from django.contrib import admin
from .models import *
# Register your models here.

class UserLocationAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at',)


admin.site.register(PhoneToken)
admin.site.register(UserRole)
admin.site.register(User)
admin.site.register(UserLocation, UserLocationAdmin)
admin.site.register(POST)
admin.site.register(Comments)
admin.site.register(Likes)
admin.site.register(ViewBy)
admin.site.register(Shop)
admin.site.register(AppLinks)