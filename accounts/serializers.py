from rest_framework import serializers
from accounts.models import *
from rest_framework.authtoken.models import Token


class UserSerializer(serializers.ModelSerializer):
    auth_token = serializers.SerializerMethodField()
    profile_pic = serializers.SerializerMethodField()
    phone_number = serializers.SerializerMethodField()
    country_code = serializers.SerializerMethodField()
    role = serializers.SerializerMethodField()
    ministry_number = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'phone_number', 'country_code', 'latitude', 'longitude',
                  'profile_pic', 'device_type', 'device_token', 'auth_token', 'role', 'health_status', 'id_number',
                  'additional_number', 'additional_email', 'ministry_approval', 'ministry_number')

    def get_role(self, instance):
        return 1 if instance.role and instance.role.name == 'User' else 2

    def get_auth_token(self, instance):
        token = Token.objects.get(user_id=instance.id)
        return token.key

    def get_profile_pic(self, user):
        request = self.context.get('request')
        return request.build_absolute_uri(user.profile_pic.url) if user.profile_pic else ""

    def get_phone_number(self, instance):
        return instance.phone_number.national_number if instance.phone_number else None

    def get_country_code(self, instance):
        return "+" + str(instance.phone_number.country_code) if instance.phone_number else ""

    def get_ministry_number(self, instance):
        request = self.context.get('request')
        if instance.phone_number:
            country_code = "+"+str(instance.phone_number.country_code)
            ministry_number = User.objects.filter(role__name="Ministry", phone_number__startswith=country_code)
            if ministry_number:
                return str(ministry_number[0].phone_number)
            else:
                return ""
        else:
            return ""


class MinistrySerializer(serializers.ModelSerializer):
    auth_token = serializers.SerializerMethodField()
    profile_pic = serializers.SerializerMethodField()
    phone_number = serializers.SerializerMethodField()
    country_code = serializers.SerializerMethodField()
    role = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'phone_number', 'country_code',
                  'profile_pic', 'device_type', 'device_token', 'auth_token', 'role', 'ministry_oof_health',
                  'city_name')

    def get_role(self, instance):
        return 1 if instance.role and instance.role.name == 'User' else 2

    def get_auth_token(self, instance):
        token = Token.objects.get(user_id=instance.id)
        return token.key

    def get_profile_pic(self, user):
        request = self.context.get('request')
        if user.profile_pic and request:
            return request.build_absolute_uri(user.profile_pic.url) if user.profile_pic else None
        return ""

    def get_phone_number(self, instance):
        return instance.phone_number.national_number if instance.phone_number else None

    def get_country_code(self, instance):
        return "+" + str(instance.phone_number.country_code) if instance.phone_number else ""


class PhoneTokenCreateSerializer(serializers.ModelSerializer):
    phone_number = serializers.CharField(required=True)
    email = serializers.SerializerMethodField()

    class Meta:
        model = PhoneToken
        fields = ('pk', 'phone_number', 'otp', 'email')

    def get_email(self, instance):
        if self.context.get('phone_number'):
            if User.objects.filter(phone_number=self.context.get('phone_number')).exists():
                user_obj = User.objects.get(phone_number=self.context.get('phone_number'))
                return user_obj.email if user_obj.email else None
            return None
        return None


class PhoneTokenValidateSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField()
    otp = serializers.CharField(max_length=40)

    class Meta:
        model = PhoneToken
        fields = ('pk', 'otp')


class LikesSerializer(serializers.ModelSerializer):
    liked_by = serializers.SerializerMethodField()

    class Meta:
        model = Likes
        fields = ("id", "liked_by", "liked_on")

    def get_liked_by(self, instance):
        liked_by = {}
        request = self.context.get('request')
        if instance.liked_by:
            liked_by['id'] = instance.liked_by.id
            liked_by['name'] = instance.liked_by.full_name if instance.liked_by.full_name else ""
        return liked_by


class CommentSerializer(serializers.ModelSerializer):
    commented_by = serializers.SerializerMethodField()
    is_liked = serializers.SerializerMethodField()

    class Meta:
        model = Comments
        fields = ("id", "comment", "commented_on", "commented_by", "likes", "is_liked")

    def get_commented_by(self, instance):
        return instance.commented_by.id

    def to_representation(self, instance):
        request = self.context.get('request')
        data = super().to_representation(instance)
        data['likes'] = instance.likes.all().count()
        if not instance.view_by.filter(comment_view_by=request.user).exists():
            instance.view_by.create(comment_view_by=request.user)
        data['view'] = instance.view_by.all().count()
        return data

    def get_is_liked(self, instance):
        request = self.context.get('request')
        return True if instance.likes.filter(liked_by=request.user).exists() else False


class PostSerializer(serializers.ModelSerializer):
    posted_by = serializers.SerializerMethodField()
    comments = CommentSerializer(many=True)
    is_liked = serializers.SerializerMethodField()

    class Meta:
        model = POST
        fields = ("id", "post",  "posted_on", "posted_by", "likes", "view", "comments", "is_liked")

    def get_posted_by(self, instance):
        return instance.posted_by.id

    def to_representation(self, instance):
        request = self.context.get('request')
        data = super().to_representation(instance)
        if data:
            data['likes'] = instance.likes.all().count()
            data['comment'] = instance.comments.all().count()
            if not instance.view.filter(comment_view_by=request.user).exists():
                instance.view.create(comment_view_by=request.user)
            data['view'] = instance.view.all().count()

            return data

    def get_is_liked(self, instance):
        request = self.context.get('request')
        return True if instance.likes.filter(liked_by=request.user).exists() else False


class ShopSerializer(serializers.ModelSerializer):

    class Meta:
        model = Shop
        fields = '__all__'


class AppLinksSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    description = serializers.SerializerMethodField()

    class Meta:
        model = AppLinks
        fields = ('id', 'android_link', 'ios_link', 'color', 'image', 'name', 'description')

    def get_name(self, instance):
        language = self.context.get('language')
        return instance.name_arabic if language and language == "Arabic" else instance.name_english

    def get_description(self, instance):
        language = self.context.get('language')
        return instance.description_arabic if language and language == "Arabic" else instance.description_english
